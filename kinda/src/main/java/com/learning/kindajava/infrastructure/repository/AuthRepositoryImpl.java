package com.learning.kindajava.infrastructure.repository;

import java.net.URISyntaxException;

import com.learning.kindajava.domain.model.AuthInfoRequest;
import com.learning.kindajava.domain.model.AuthInfoResponse;
import com.learning.kindajava.domain.repository.AuthRepository;
import com.learning.kindajava.infrastructure.client.LoginClient;
import com.learning.kindajava.infrastructure.dto.AuthRequestParam;
import com.learning.kindajava.infrastructure.dto.AuthResponseParam;

import org.springframework.stereotype.Repository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class AuthRepositoryImpl implements AuthRepository {

    private final LoginClient client;

    /**
     * {@inheritDoc}
     * 
     * @throws URISyntaxException
     */
    @Override
    public AuthInfoResponse doLogin(AuthInfoRequest authInfoRequest) {

        // client呼出
        AuthResponseParam res = 
            this.client.execute(this.createAuthRequestParam(authInfoRequest));

        return this.setAuthInfoResponse(res);
    }

    /**
     * AuthRequestParam生成。
     * 
     * @param authRequest 認証処理
     * @return AuthRequestParam
     */
    private AuthRequestParam createAuthRequestParam(AuthInfoRequest request) {
        return new AuthRequestParam(
            request.getId(),
            request.getPassword()
        );
    }

    /**
     * 認証情報設定。
     * 
     * @param response 認証情報レスポンス
     * @param headers HTTPヘッダー
     */
    private AuthInfoResponse setAuthInfoResponse(AuthResponseParam authResponseParam) {
        return new AuthInfoResponse(
            authResponseParam.getCommondto(),
            authResponseParam.getUserid(),
            authResponseParam.getUsername(),
            authResponseParam.getRole()
        );
    }
}