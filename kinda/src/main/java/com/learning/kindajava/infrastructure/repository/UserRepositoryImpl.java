package com.learning.kindajava.infrastructure.repository;

import com.learning.kindajava.domain.model.User;
import com.learning.kindajava.domain.repository.UserRepository;
import com.learning.kindajava.infrastructure.entity.UserEntity;

import org.springframework.stereotype.Repository;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class UserRepositoryImpl implements UserRepository {

    @NonNull
    private UserJpaRepository jpaRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public User findById(String id) {
        return this.jpaRepository.findById(id).map(UserEntity::toDomainUser).orElse(null);
    }

    /**
     * {@inheritDoc}
     */
    public User save(User user) {
        return this.jpaRepository.save(UserEntity.build(user)).toDomainUser();
    }

    /**
     * {@inheritDoc}
     */
    public void deleteById(String id) {
        this.jpaRepository.deleteById(id);
    }
}