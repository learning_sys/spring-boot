package com.learning.kindajava.infrastructure.dto;

import com.learning.kindajava.common.dto.CommonDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AuthResponseParam {

    /** 共通DTO */
    private CommonDto commondto;

    /** ユーザID */
    private String userid;

    /** ユーザ名 */
    private String username;

    /** 権限 */
    private String role;
}