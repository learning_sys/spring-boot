package com.learning.kindajava.infrastructure.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.learning.kindajava.domain.model.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "test_users")
public class UserEntity {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "value")
    private String value;

    public static UserEntity build(User user) {
        return UserEntity.builder().id(user.getId()).value(user.getValue()).build();
    }

    public User toDomainUser() {
        return User.builder().id(this.id).value(this.value).build();
    }
}