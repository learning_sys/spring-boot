package com.learning.kindajava.infrastructure.repository;

import com.learning.kindajava.infrastructure.entity.UserEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserJpaRepository extends JpaRepository<UserEntity, String> {
}