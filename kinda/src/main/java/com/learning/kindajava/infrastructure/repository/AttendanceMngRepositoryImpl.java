package com.learning.kindajava.infrastructure.repository;

import static com.learning.kindajava.common.Constant.ResultCd.ERROR;
import static com.learning.kindajava.common.Constant.ResultCd.SUCCESS;
import static com.learning.kindajava.common.Utility.getEndDay;
import static com.learning.kindajava.common.Utility.getFirstDay;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.learning.kindajava.common.dto.CommonDto;
import com.learning.kindajava.common.dto.ResultDto;
import com.learning.kindajava.domain.model.AttendanceInfo;
import com.learning.kindajava.domain.model.AttendanceInfoRequest;
import com.learning.kindajava.domain.model.AttendanceInfoResponse;
import com.learning.kindajava.domain.repository.AttendanceMngRepository;
import com.learning.kindajava.infrastructure.entity.AttendanceMngEntity;
import com.learning.kindajava.infrastructure.entity.AttendanceMngKey;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Transactional
@Repository
public class AttendanceMngRepositoryImpl implements AttendanceMngRepository {

    @NonNull
    private AttendanceMngJpaRepository jpaRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public AttendanceInfoResponse read(AttendanceInfoRequest request) {

        // 参照実施
        List<AttendanceMngEntity> entityList
            = this.findByAttendanceInfoList(request.getAttendanceInfoList());

        return this.createAttendanceInfoResponse(entityList, SUCCESS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AttendanceInfoResponse readMonthly(AttendanceInfoRequest request) {

        // 参照実施
        Date attendanceDate = request.getAttendanceDate(0);
        List<AttendanceMngEntity> entityList =
            this.jpaRepository.findByPrimaryKey_StaffIdAndPrimaryKey_AttendanceDateBetween(
                request.getStaffId(0), getFirstDay(attendanceDate), getEndDay(attendanceDate));

        return this.createAttendanceInfoResponse(entityList, SUCCESS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AttendanceInfoResponse create(AttendanceInfoRequest request) {
        List<AttendanceMngEntity> entityList = new ArrayList<>();

        String resultCd = SUCCESS;

        for (AttendanceInfo info : request.getAttendanceInfoList()) {

            // 登録前確認
            if(StringUtils.isNotEmpty(this.findByAttendanceInfo(info).getStaffId())) {

                // 一意制約エラー
                resultCd = ERROR;
                continue;
            }

            // 登録実施
            entityList.add(this.jpaRepository.save(new AttendanceMngEntity(info)));
        }

        return this.createAttendanceInfoResponse(entityList, resultCd);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AttendanceInfoResponse update(AttendanceInfoRequest request) {
        List<AttendanceMngEntity> entityList = new ArrayList<>();

        String resultCd = SUCCESS;

        for (AttendanceInfo info : request.getAttendanceInfoList()) {

            // 更新前確認
            if(StringUtils.isEmpty(this.findByAttendanceInfo(info).getStaffId())) {

                // 更新対象なし
                resultCd = ERROR;
                continue;
            }

            // 更新実施
            entityList.add(this.jpaRepository.save(new AttendanceMngEntity(info)));
        }

        return this.createAttendanceInfoResponse(entityList, resultCd);
    }

    /**
     * データ参照(複数リクエスト)。
     * 
     * @param request 勤怠情報リスト
     * @return 参照結果
     */
    private List<AttendanceMngEntity> findByAttendanceInfoList(List<AttendanceInfo> requestList) {
        List<AttendanceMngEntity> entityList = new ArrayList<>();
        for (AttendanceInfo request : requestList) {
            entityList.add(this.findByAttendanceInfo(request));
        }
        return entityList;
    }

    /**
     * データ参照。
     * 
     * @param request 勤怠情報
     * @return 参照結果
     */
    private AttendanceMngEntity findByAttendanceInfo(AttendanceInfo request) {
        AttendanceMngKey key = new AttendanceMngKey(
            request.getStaffId(),
            request.getAttendanceDate()
        );
        return this.jpaRepository.findById(key).orElse(new AttendanceMngEntity());
    }

    /**
     * 勤怠情報モデルレスポンス生成。
     * 
     * @param entity 勤怠管理エンティティ
     * @param resultCd 登録結果
     * @return 勤怠情報モデルレスポンス
     */
    private AttendanceInfoResponse createAttendanceInfoResponse(
        List<AttendanceMngEntity> entityList, String resultCd) {
        return new AttendanceInfoResponse(
            new CommonDto(new ResultDto(resultCd)),
            this.createAttendanceInfoList(entityList)
        );
    }

    /**
     * 勤怠情報リスト生成。
     * 
     * @param entity 勤怠管理エンティティ
     * @param resultCd 登録結果
     * @return 勤怠情報モデルレスポンス
     */
    private List<AttendanceInfo> createAttendanceInfoList(List<AttendanceMngEntity> entityList) {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        for (AttendanceMngEntity entity : entityList) {
            attendanceInfoList.add(
                new AttendanceInfo(
                    entity.getPrimaryKey().getStaffId(),
                    entity.getPrimaryKey().getAttendanceDate(),
                    entity.getAttendanceDiv(),
                    entity.getAttendanceDivDetail(),
                    entity.getPunchInTime(),
                    entity.getPunchOutTime(),
                    entity.getRestTime(),
                    entity.getAttendanceReason()
                )
            );
        }
        return attendanceInfoList;
    }
}