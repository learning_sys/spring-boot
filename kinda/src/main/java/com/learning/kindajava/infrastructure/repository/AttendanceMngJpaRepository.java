package com.learning.kindajava.infrastructure.repository;

import java.util.Date;
import java.util.List;

import com.learning.kindajava.infrastructure.entity.AttendanceMngEntity;
import com.learning.kindajava.infrastructure.entity.AttendanceMngKey;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AttendanceMngJpaRepository extends JpaRepository<AttendanceMngEntity, AttendanceMngKey> {

    /**
     * 指定月勤怠情報参照。
     * 
     * @param staffId 社員ID
     * @param fromDate 指定月月初日
     * @param toDate 指定月月末日
     * @return 指定月勤怠情報
     */
    public List<AttendanceMngEntity> findByPrimaryKey_StaffIdAndPrimaryKey_AttendanceDateBetween(
        String staffId, Date fromDate, Date toDate);
}
