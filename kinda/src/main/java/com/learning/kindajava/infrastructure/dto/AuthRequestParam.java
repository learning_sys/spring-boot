package com.learning.kindajava.infrastructure.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class AuthRequestParam {

    /** ユーザID */
    private String id;

    /** パスワード */
    private String pass;
}