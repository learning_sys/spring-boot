package com.learning.kindajava.application.controller;

import com.learning.kindajava.application.payload.UserResponse;
import com.learning.kindajava.domain.service.UserService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
@RestController
public class UserRestController {

    @NonNull
    private final UserService service;

    @GetMapping("/read")
    private UserResponse findById(@RequestParam(name = "id") String id) {
        return UserResponse.build(this.service.findById(id));
    }
}