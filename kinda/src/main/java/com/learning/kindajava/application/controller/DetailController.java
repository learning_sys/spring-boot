package com.learning.kindajava.application.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 詳細入力画面のコントローラ。
 */
@Controller
public class DetailController {

    /**
     * 詳細入力画面生成。
     * @param model モデル
     * @return 詳細入力画面
     */
    @GetMapping("/detail")
    private String getDetailPage(Model model) {
        return "pages/detail";
    }
}
