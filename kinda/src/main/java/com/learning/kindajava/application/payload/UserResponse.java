package com.learning.kindajava.application.payload;

import com.learning.kindajava.domain.model.User;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserResponse {

    /** ユーザID */
    private String id;

    /** ユーザ情報 */
    private String value;

    public static UserResponse build(User user) {
        return UserResponse.builder().id(user.getId()).value(user.getValue()).build();
    }

    public User toDomainUser() {
        return User.builder().id(this.id).value(this.value).build();
    }
}