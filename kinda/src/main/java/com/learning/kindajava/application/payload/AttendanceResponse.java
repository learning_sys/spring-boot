package com.learning.kindajava.application.payload;

import java.util.List;

import com.learning.kindajava.common.dto.CommonDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AttendanceResponse {

	/** 共通DTO */
    private CommonDto commonDto;

    // 勤怠ペイロードリスト
    private List<AttendancePayload> attendancePayloadList;
}