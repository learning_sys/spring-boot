package com.learning.kindajava.application.payload;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AttendanceRequest {

    // 操作区分
    private String operateDiv;

    // 勤怠ペイロードリスト
    private List<AttendancePayload> attendancePayloadList;
}