package com.learning.kindajava.common.configuration;

import com.learning.kindajava.common.interceptor.HttpRequestHandlerInterceptor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class RequestHandlerConfig extends WebMvcConfigurationSupport {

    @Bean
    public HttpRequestHandlerInterceptor requestHandlerInterceptor() {
        return new HttpRequestHandlerInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this.requestHandlerInterceptor())
            .addPathPatterns("/*")
            .excludePathPatterns("/login", "/logout", "/dologin", "/error", "/resources/**");
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}