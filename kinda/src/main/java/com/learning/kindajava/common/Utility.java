package com.learning.kindajava.common;

import static com.learning.kindajava.common.Constant.CookieItem.ROLE;
import static com.learning.kindajava.common.Constant.CookieItem.SECRET_KEY;
import static com.learning.kindajava.common.Constant.CookieItem.USER_NAME;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.learning.kindajava.common.Constant.Role;

import org.springframework.util.StringUtils;

public class Utility {

    /**
     * コンストラクタ。 インスタンス化させない為private。
     */
    private Utility() {
    }

    /**
     * String型からDate型への変換。
     * 
     * @param date 変換対象日付
     * @param format 日付フォーマット
     * @return 変換後日付
     * @throws ParseException
     */
    public static Date string2Date(String date, String format) throws ParseException {
        if (StringUtils.isEmpty(date)) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.parse(date);
    }

    /**
     * Date型からString型への変換。
     * 
     * @param date 変換対象日付
     * @param format 日付フォーマット
     * @return 変換後日付
     * @throws ParseException
     */
    public static String date2String(Date date, String format) throws ParseException {
        if (date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    /**
     * 月初日付取得。
     * 
     * @param date 対象月
     * @return 月初日付
     */
    public static Date getFirstDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = 1;
        
        cal.set(year, month, day, 0, 0, 0);
        return cal.getTime();
    }

    /**
     * 月末日付取得。
     * 
     * @param date 対象月
     * @return 月末日付
     */
    public static Date getEndDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.getActualMaximum(Calendar.DATE);
        
        cal.set(year, month, day, 0, 0, 0);
        return cal.getTime();
    }

    /**
     * Cookie取得。
     * 
     * @param request    HttpServletRequest
     * @param cookieName cookie名
     * @return cookie
     * @throws UnsupportedEncodingException
     */
    public static String getCookie(HttpServletRequest request, String cookieName)
        throws UnsupportedEncodingException {
        String cookieValue = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookieName.equals(cookie.getName())) {
                    cookieValue = URLDecoder.decode(cookie.getValue(), StandardCharsets.UTF_8.name());
                }
            }
        }
        return cookieValue;
    }

    /**
     * Cookie設定。
     * 
     * @param response    HttpServletResponse
     * @param cookieName  cookie名
     * @param cookieValue cookie値
     * @throws UnsupportedEncodingException
     */
    public static void setCookie(HttpServletResponse response, String cookieName, String cookieValue)
        throws UnsupportedEncodingException {
        Cookie cookie = new Cookie(cookieName, URLEncoder.encode(cookieValue, StandardCharsets.UTF_8.name()));
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    /**
     * Cookie全削除。
     * 
     * @param response HttpServletResponse
     * @param cookieName cookie名
     */
    public static void removeAllCookie(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                cookie.setMaxAge(0);
                cookie.setPath("/");
                response.addCookie(cookie);
            }
        }
    }

    /**
     * 管理者権限判定。
     * 
     * @param role 権限
     * @return true:管理者権限 false:ユーザ権限
     */
    public static boolean isAdmin(String role) {
        return Role.ADMIN.equals(role);
    }

    /**
     * jwt token生成。
     * 
     * @param userName ログインユーザ名
     * @param role ログインユーザロール
     * @return token
     */
    public static String createJwtToken(String userName, String role) {
        String token = null;
        try {
            Date issuedTime = new Date();
            Date expireTime = new Date();
            expireTime.setTime(expireTime.getTime() + 1800000l);

            Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY);
            token = JWT.create()
                .withIssuer("auth0")
                .withIssuedAt(issuedTime)
                .withExpiresAt(expireTime)
                .withClaim(USER_NAME, userName)
                .withClaim(ROLE, role)
                .sign(algorithm);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return token;
    }
}