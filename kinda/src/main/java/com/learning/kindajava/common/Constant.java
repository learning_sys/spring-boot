package com.learning.kindajava.common;

public class Constant {

    /**
     * コンストラクタ。
     * インスタンス化させない為private。
     */
    private Constant() {
    }

    /** 日付フォーマット */
    public static class DateFormat {
        /** yyyyMMdd */
        public static final String YYYYMMDD = "yyyyMMdd";
        /** yyyy-MM-dd */
        public static final String YYYYMMDD_HYPHEN = "yyyy-MM-dd";
        /** hh:mm:ss */
        public static final String HHMMSS = "HH:mm:ss";
    }

    /** 結果コード */
    public static class ResultCd {
        /** 結果コード（正常） */
        public static final String SUCCESS = "0";
        /** 結果コード（警告） */
        public static final String WARNING = "3";
        /** 結果コード（異常） */
        public static final String ERROR = "6";
    }

    /** DB操作区分コード */
    public static class OperateDiv {
        /** 登録 */
        public static final String CREATE = "C";
        /** 参照（日） */
        public static final String READ_DAILY = "RD";
        /** 参照（月） */
        public static final String READ_MONTHLY = "RM";
        /** 更新 */
        public static final String UPDATE = "U";
        /** 削除 */
        public static final String DELETE = "D";
    }

    /** カテゴリ－コード */
    public static class CategoryCd {
        /** 勤怠区分 */
        public static final String ATTENDANCE = "100";
        /** 勤怠詳細区分 */
        public static final String ATTENDANCE_DETAIL = "200";
    }

    /** 勤怠区分コード */
    public static class AttendanceCd {
        /** 出勤 */
        public static final String PUNCH_IN = "001";
        /** 有休 */
        public static final String PAID_LEAVE = "002";
        /** 欠勤 */
        public static final String ABSENCE = "003";
        /** 退勤 */
        public static final String PUNCH_OUT = "004";
    }

    /** 勤怠詳細区分コード */
    public static class AttendanceDetailCd {
        /** 勤怠詳細区分_遅刻 */
        public static final String ATTENDANCE_DETAIL_LATE = "001";
        /** 勤怠詳細区分_早退 */
        public static final String ATTENDANCE_DETAIL_EARLY = "002";
        /** 勤怠詳細区分_遅刻早退 */
        public static final String ATTENDANCE_DETAIL_LATE_AND_EARLY = "003";
    }

    /** クッキー名 */
    public static class CookieItem {
        /** JWTトークン */
        public static final String APP_TOKEN = "app_token";
        /** 秘密鍵 */
        public static final String SECRET_KEY = "secret_key";
        /** ユーザID */
        public static final String USER_ID = "user_id";
        /** ユーザ名 */
        public static final String USER_NAME = "user_name";
        /** 権限 */
        public static final String ROLE = "role";
    }

    /** 権限 */
    public static class Role {
        /** 管理者 */
        public static final String ADMIN = "ROLE_ADMIN";
        /** ユーザ */
        public static final String USER = "ROLE_USER";
    }
}