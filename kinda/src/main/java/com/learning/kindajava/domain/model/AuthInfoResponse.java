package com.learning.kindajava.domain.model;

import com.learning.kindajava.common.dto.CommonDto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class AuthInfoResponse {

    /** 共通DTO */
    private CommonDto commonDto;

    /** ユーザID */
    private String userId;

    /** ユーザ名 */
    private String userName;

    /** 権限 */
    private String role;
}