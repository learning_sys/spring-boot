package com.learning.kindajava.domain.service;

import com.learning.kindajava.domain.model.AuthInfoRequest;
import com.learning.kindajava.domain.model.AuthInfoResponse;
import com.learning.kindajava.domain.repository.AuthRepository;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class AuthService {

    private final AuthRepository repository;

    public AuthInfoResponse doLogin(AuthInfoRequest authInfoRequest) {

        // repository呼出
        return repository.doLogin(authInfoRequest);
    }
}