package com.learning.kindajava.domain.repository;

import com.learning.kindajava.domain.model.AuthInfoRequest;
import com.learning.kindajava.domain.model.AuthInfoResponse;

public interface AuthRepository {

    /**
     * ログイン認証。
     * 
     * @param authInfoRequest 認証情報リクエスト
     */
    public AuthInfoResponse doLogin(AuthInfoRequest authInfoRequest);
}