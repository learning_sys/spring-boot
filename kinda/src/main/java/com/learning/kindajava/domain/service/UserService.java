package com.learning.kindajava.domain.service;

import com.learning.kindajava.domain.model.User;
import com.learning.kindajava.domain.repository.UserRepository;

import org.springframework.stereotype.Service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class UserService {

    @NonNull
    private final UserRepository repository;

    /**
     * ユーザ検索。
     * 
     * @param id ユーザID
     * @return ユーザ情報
     */
    public User findById(String id) {
        return this.repository.findById(id);
    }

    /**
     * ユーザ作成、更新。
     * 
     * @param user ユーザ情報
     * @return 更新後のユーザ情報
     */
    public User save(User user) {
        return this.repository.save(user);
    }

    /**
     * ユーザ削除。
     * 
     * @param id ユーザID
     */
    public void deleteById(String id) {
        this.repository.deleteById(id);
    }
}