package com.learning.kindajava.domain.repository;

import com.learning.kindajava.domain.model.User;

public interface UserRepository {

    /**
     * ユーザ検索。
     * 
     * @param id ユーザID
     * @return ユーザ情報
     */
    public User findById(String id);

    /**
     * ユーザ作成、更新。
     * 
     * @param user ユーザ情報
     * @return 更新後のユーザ情報
     */
    public User save(User user);

    /**
     * ユーザ削除。
     * 
     * @param id ユーザID
     */
    public void deleteById(String id);
}