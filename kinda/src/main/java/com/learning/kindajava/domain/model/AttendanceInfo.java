package com.learning.kindajava.domain.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AttendanceInfo {

    /** 社員ID */
    private String staffId;

    /** 日付 */
    private Date attendanceDate;

    /** 勤怠区分 */
    private String attendanceDiv;

    /** 勤怠区分詳細 */
    private String attendanceDivDetail;

    /** 出勤時刻 */
    private Date punchInTime;

    /** 退勤時刻 */
    private Date punchOutTime;

    /** 休憩時間 */
    private int restTime;

    /** 勤怠理由 */
    private String attendanceReason;
}