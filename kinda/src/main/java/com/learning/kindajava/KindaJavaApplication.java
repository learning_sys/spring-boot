package com.learning.kindajava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class KindaJavaApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(KindaJavaApplication.class, args);
	}
}
