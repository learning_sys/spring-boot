package com.learning.kindajava.application.controller;

import static com.learning.kindajava.common.Constant.CookieItem.APP_TOKEN;
import static com.learning.kindajava.common.Constant.ResultCd.ERROR;
import static com.learning.kindajava.common.Constant.ResultCd.SUCCESS;
import static com.learning.kindajava.common.Constant.ResultCd.WARNING;
import static com.learning.kindajava.common.Constant.CookieItem.ROLE;
import static com.learning.kindajava.common.Constant.CookieItem.USER_NAME;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.cookie;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import com.learning.kindajava.common.dto.CommonDto;
import com.learning.kindajava.common.dto.ResultDto;
import com.learning.kindajava.domain.model.AuthInfoRequest;
import com.learning.kindajava.domain.model.AuthInfoResponse;
import com.learning.kindajava.domain.service.AuthService;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AuthController.class)
public class AuthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthService service;

    @Nested
    @DisplayName("ログイン認証")
    class doLogin {

        @Test
        @DisplayName("認証成功時、ホーム画面へのリダイレクトを行うこと")
        void normalTest() throws Exception {

            // setup
            AuthInfoRequest request = new AuthInfoRequest(null, null);
            doReturn(this.creatResponseStub(SUCCESS)).when(service).doLogin(request);

            // execute && assert
            mockMvc.perform(get("/dologin"))
                .andExpect(redirectedUrl("/home"));
        }

        @Test
        @DisplayName("認証失敗時、cookieを削除し、ログイン画面へのリダイレクトを行うこと")
        void failureTest() throws Exception {

            // setup
            AuthInfoRequest request = new AuthInfoRequest(null, null);
            doReturn(this.creatResponseStub(WARNING)).when(service).doLogin(request);

            // execute && assert
            mockMvc.perform(get("/dologin"))
                .andExpect(cookie().doesNotExist(USER_NAME))
                .andExpect(cookie().doesNotExist(ROLE))
                .andExpect(cookie().doesNotExist(APP_TOKEN))
                .andExpect(redirectedUrl("/login?error"));
        }

        @Test
        @DisplayName("認証中エラー発生時、cookieを削除し、エラー画面へのリダイレクトを行うこと")
        void errorTest() throws Exception {

            // setup
            AuthInfoRequest request = new AuthInfoRequest(null, null);
            doReturn(this.creatResponseStub(ERROR)).when(service).doLogin(request);

            // execute && assert
            mockMvc.perform(get("/dologin"))
                .andExpect(cookie().doesNotExist(USER_NAME))
                .andExpect(cookie().doesNotExist(ROLE))
                .andExpect(cookie().doesNotExist(APP_TOKEN))
                .andExpect(redirectedUrl("/error"));
        }

        /**
         * レスポンス生成。
         * 
         * @param resultCd 結果コード
         * @return レスポンス
         */
        private AuthInfoResponse creatResponseStub(String resultCd) {
            if (SUCCESS.equals(resultCd)) {
                return new AuthInfoResponse(
                    new CommonDto(new ResultDto(resultCd)),
                    "000000",
                    "testUser",
                    "ROLE_USER"
                );
            }else {
                return new AuthInfoResponse(
                    new CommonDto(new ResultDto(resultCd)),
                    null,
                    null,
                    null
                );
            }
        }
    }

    @Nested
    @DisplayName("ログアウト処理")
    class doLogout {

        @Test
        @DisplayName("ログアウト処理時、cookieを削除し、ログイン画面へのリダイレクトを行うこと")
        void normalTest() throws Exception {

            // execute && assert
            mockMvc.perform(get("/logout"))
                .andExpect(cookie().doesNotExist(USER_NAME))
                .andExpect(cookie().doesNotExist(ROLE))
                .andExpect(cookie().doesNotExist(APP_TOKEN))
                .andExpect(redirectedUrl("/login"));
        }
    }
}