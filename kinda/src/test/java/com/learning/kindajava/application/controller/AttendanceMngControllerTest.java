package com.learning.kindajava.application.controller;

import static com.learning.kindajava.common.Constant.AttendanceCd.PUNCH_IN;
import static com.learning.kindajava.common.Constant.CookieItem.USER_ID;
import static com.learning.kindajava.common.Constant.ResultCd.SUCCESS;
import static com.learning.kindajava.common.Utility.date2String;
import static com.learning.kindajava.common.Utility.string2Date;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.learning.kindajava.application.payload.AttendancePayload;
import com.learning.kindajava.application.payload.AttendanceRequest;
import com.learning.kindajava.application.payload.AttendanceResponse;
import com.learning.kindajava.common.Constant.DateFormat;
import com.learning.kindajava.common.Constant.OperateDiv;
import com.learning.kindajava.common.dto.CommonDto;
import com.learning.kindajava.common.dto.ResultDto;
import com.learning.kindajava.domain.model.AttendanceInfo;
import com.learning.kindajava.domain.model.AttendanceInfoRequest;
import com.learning.kindajava.domain.model.AttendanceInfoResponse;
import com.learning.kindajava.domain.service.AttendanceMngService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AttendanceMngController.class)
public class AttendanceMngControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AttendanceMngService service;

    ObjectMapper mapper;

    @BeforeEach
    void setup() {

        // nullフィールドを無視
        mapper = new ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    @Test
    @DisplayName("/attendance/operateリクエスト（GET）時、サービスを呼出し、レスポンスを画面へ返却すること")
    void operateGetTest() throws Exception {

        // setup
        Date date = new Date();
        String operateDiv = OperateDiv.READ_DAILY;
        String attendanceDate = date2String(date, DateFormat.YYYYMMDD_HYPHEN);
        doReturn(this.createAttendanceInfoResponseStub(date))
            .when(this.service).operateAttendanceInfo(
                this.createAttendanceInfoRequestStub(operateDiv, attendanceDate));

        // execute && assert
        MvcResult result = this.mockMvc.perform(get("/api/v1/attendance/operate")
            .param("operateDiv", operateDiv)
            .param("attendanceDate", attendanceDate)
            .cookie(new Cookie(USER_ID, "000000")))
            .andExpect(status().isOk()).andReturn();

        // assert
        assertEquals(this.createAttendanceResponseStub(date),
            mapper.readValue(result.getResponse().getContentAsString(), AttendanceResponse.class));
    }

    @Test
    @DisplayName("/attendance/operateリクエスト（POST）時、サービスを呼出し、レスポンスを画面へ返却すること")
    void operatePostTest() throws Exception {

        // setup
        Date date = new Date();
        String jsonReq = this.mapper.writeValueAsString(this.createAttendanceRequestStub(date));
        doReturn(this.createAttendanceInfoResponseStub(date))
            .when(this.service).operateAttendanceInfo(this.createAttendanceInfoRequestStub(date));

        // execute && assert
        MvcResult result = this.mockMvc.perform(post("/api/v1/attendance/operate")
            .contentType(MediaType.APPLICATION_JSON).content(jsonReq).cookie(new Cookie(USER_ID, "000000")))
            .andExpect(status().isOk()).andReturn();

        // assert
        assertEquals(this.createAttendanceResponseStub(date),
            mapper.readValue(result.getResponse().getContentAsString(), AttendanceResponse.class));
    }

    /**
     * 勤怠リクエスト情報スタブ作成
     * 
     * @param date 日付
     * @return 勤怠リクエスト情報スタブ
     * @throws ParseException
     */
    private AttendanceRequest createAttendanceRequestStub(Date date) throws ParseException {
        return new AttendanceRequest(OperateDiv.CREATE, this.createAttendancePayloadListStub(date));
    }

    /**
     * 勤怠ペイロードリストスタブ作成
     * 
     * @param date 日付
     * @return 勤怠ペイロードリストスタブ
     * @throws ParseException
     */
    private List<AttendancePayload> createAttendancePayloadListStub(Date date)
        throws ParseException {
        List<AttendancePayload> attendancePayloadList = new ArrayList<>();
        attendancePayloadList.add(
            new AttendancePayload(
                "000000",
                date2String(date, DateFormat.YYYYMMDD_HYPHEN),
                PUNCH_IN,
                null,
                date2String(date, DateFormat.HHMMSS),
                null,
                0,
                null
            )
        );
        return attendancePayloadList;
    }

    /**
     * 勤怠情報リクエスト情報スタブ生成。
     * 
     * @param operateDiv 勤怠区分
     * @param attendanceDate 日付
     * @return AttendanceInfoRequest
     * @throws ParseException
     */
    private AttendanceInfoRequest createAttendanceInfoRequestStub(
        String operateDiv, String attendanceDate) throws ParseException {
        return new AttendanceInfoRequest(
            operateDiv,
            this.createAttendanceInfoListStub(attendanceDate)
        );
    }

    /**
     * 勤怠情報リクエスト情報スタブ生成。
     * 
     * @param date 日付
     * @return AttendanceInfoRequest
     * @throws ParseException
     */
    private AttendanceInfoRequest createAttendanceInfoRequestStub(Date date)
        throws ParseException {
        return new AttendanceInfoRequest(
            OperateDiv.CREATE,
            this.createAttendanceInfoListStub(date)
        );
    }

    /**
     * 勤怠情報リストスタブ生成。
     * 
     * @param attendanceDate 日付
     * @return 勤怠情報リストスタブ
     * @throws ParseException
     */
    private List<AttendanceInfo> createAttendanceInfoListStub(String attendanceDate)
        throws ParseException {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        attendanceInfoList.add(
            new AttendanceInfo(
                "000000",
                string2Date(attendanceDate, DateFormat.YYYYMMDD_HYPHEN),
                null,
                null,
                null,
                null,
                0,
                null
            )
        );
        return attendanceInfoList;
    }

    /**
     * 勤怠情報リストスタブ生成。
     * 
     * @param date 日付
     * @return 勤怠情報リストスタブ
     * @throws ParseException
     */
    private List<AttendanceInfo> createAttendanceInfoListStub(Date date) throws ParseException {
        String attendanceDate = date2String(date, DateFormat.YYYYMMDD_HYPHEN);
        String punchInTime = date2String(date, DateFormat.HHMMSS);
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        attendanceInfoList.add(
            new AttendanceInfo(
                "000000",
                string2Date(attendanceDate, DateFormat.YYYYMMDD_HYPHEN),
                PUNCH_IN,
                null,
                string2Date(punchInTime, DateFormat.HHMMSS),
                null,
                0,
                null
            )
        );
        return attendanceInfoList;
    }

    /**
     * 勤怠情報レスポンス情報スタブ生成。
     * 
     * @param date 日付
     * @return 勤怠情報レスポンス情報スタブ
     * @throws ParseException
     */
    private AttendanceInfoResponse createAttendanceInfoResponseStub(Date date)
        throws ParseException {
        return new AttendanceInfoResponse(
            new CommonDto(new ResultDto(SUCCESS)),
            this.createAttendanceInfoListStub(date)
        );
    }

    /**
     * 勤怠レスポンス情報スタブ作成
     * 
     * @param date 日付
     * @return 勤怠レスポンス情報スタブ
     * @throws ParseException
     */
    private AttendanceResponse createAttendanceResponseStub(Date date) throws ParseException {
        return new AttendanceResponse(
            new CommonDto(new ResultDto(SUCCESS)),
            this.createAttendancePayloadListStub(date)
        );
    }
}