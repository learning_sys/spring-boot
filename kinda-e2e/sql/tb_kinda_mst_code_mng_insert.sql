insert into tb_kinda_mst_code_mng(category_cd, category_val, code, value, created_at, create_user, updated_at, update_user)
values
('100','勤怠区分','001','出勤',current_timestamp,'999999',current_timestamp,'999999'),
('100','勤怠区分','002','有休',current_timestamp,'999999',current_timestamp,'999999'),
('100','勤怠区分','003','欠勤',current_timestamp,'999999',current_timestamp,'999999'),
('200','勤怠区分詳細','001','遅刻',current_timestamp,'999999',current_timestamp,'999999'),
('200','勤怠区分詳細','002','早退',current_timestamp,'999999',current_timestamp,'999999'),
('200','勤怠区分詳細','003','遅刻早退',current_timestamp,'999999',current_timestamp,'999999');
