create table tb_kinda_mst_code_mng (
  category_cd varchar(3) not null
  , category_val varchar(255) not null
  , code varchar(3) not null
  , value varchar(255) not null
  , created_at timestamp not null
  , create_user varchar(255) not null
  , updated_at timestamp not null
  , update_user varchar(255) not null
  , primary key (category_cd, code)
);